ruby-diaspora-federation (0.2.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Adds support for Ruby 3.0 and Rails < 7.
  * d/control (Standards-Version): Bump to 4.6.0.
    (Homepage): Update URL.
  * d/copyright (Source): Update URL.
    (Copyright): Add team.
  * d/rules: Use gem installation layout and install upstream changelog.
  * d/tests/control.ex: Remove example template.
  * d/upstream/metadata: Update URLs.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 25 Nov 2021 19:25:50 +0100

ruby-diaspora-federation (0.2.6-4) unstable; urgency=medium

  * Update patch for faraday 1.0 compat with changes merged upstream

 -- Pirate Praveen <praveen@debian.org>  Tue, 29 Jun 2021 22:09:11 +0530

ruby-diaspora-federation (0.2.6-3) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Pirate Praveen ]
  * Relax dependency on faraday in gemspec to allow version 1 (Closes: #976343)
  * Bump Standards-Version to 4.5.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Fri, 25 Jun 2021 15:03:51 +0530

ruby-diaspora-federation (0.2.6-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Lucas Kanashiro ]
  * Add patch to relax dep version constraint in gemspec
  * Bump debhelper compatibility level to 13
  * Declare compliance with Debian Policy 4.5.0
  * Do not runtime depend on the interpreter

 -- Lucas Kanashiro <kanashiro@debian.org>  Wed, 01 Jul 2020 17:59:13 -0300

ruby-diaspora-federation (0.2.6-1) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Samyak Jain ]
  * New upstream version 0.2.6
  * Remove d/compat and update d/control wrt cme
  * Update version=4 in d/watch
  * Adds metadata in d/upstream

 -- Samyak Jain <samyak.jn11@gmail.com>  Sun, 27 Oct 2019 10:29:28 +0530

ruby-diaspora-federation (0.2.5-2) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.3.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Thu, 07 Feb 2019 12:49:01 +0530

ruby-diaspora-federation (0.2.5-1) experimental; urgency=medium

  * Team upload.
  * New upstream release
  * Use salsa.debian.org in Vcs-* fields
  * Bump debhelper compatibility level to 11
  * Move debian/watch to gemwatch.debian.net

 -- Mangesh Divate <divatemangesh12@gmail.com>  Mon, 05 Nov 2018 17:31:23 +0000

ruby-diaspora-federation (0.2.1-1) experimental; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Tue, 19 Sep 2017 01:09:15 +0530

ruby-diaspora-federation (0.1.9-1) experimental; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Tue, 27 Jun 2017 21:02:06 +0530

ruby-diaspora-federation (0.1.7-1) experimental; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Mon, 16 Jan 2017 23:07:57 +0530

ruby-diaspora-federation (0.1.4-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Wed, 09 Nov 2016 14:07:34 +0530

ruby-diaspora-federation (0.1.4-1) experimental; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Sep 2016 10:24:45 +0530

ruby-diaspora-federation (0.0.13-1) unstable; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sat, 09 Jul 2016 20:26:37 +0530

ruby-diaspora-federation (0.0.12-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Fri, 08 Jul 2016 23:33:11 +0530

ruby-diaspora-federation (0.0.12-1) experimental; urgency=medium

  * New upstream release
  * Bump standards version to 3.9.7 (no changes)
  * Switch vcs url to https

 -- Pirate Praveen <praveen@debian.org>  Fri, 04 Mar 2016 14:19:04 +0530

ruby-diaspora-federation (0.0.10-1) experimental; urgency=medium

  * Team upload.
  * New upstream release

 -- Balasankar C <balasankarc@autistici.org>  Fri, 01 Jan 2016 16:32:16 +0530

ruby-diaspora-federation (0.0.8-2) unstable; urgency=medium

  * Re-upload to unstable

 -- Pirate Praveen <praveen@debian.org>  Tue, 15 Dec 2015 00:31:46 +0530

ruby-diaspora-federation (0.0.8-1) experimental; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Fri, 30 Oct 2015 03:25:49 +0530

ruby-diaspora-federation (0.0.6-1) experimental; urgency=medium

  * New upstream release
  * Add new dependencies: ruby-valid, ruby-faraday, ruby-typhoeus,
    ruby-faraday-middleware

 -- Pirate Praveen <praveen@debian.org>  Tue, 15 Sep 2015 20:03:19 +0530

ruby-diaspora-federation (0.0.3-1) experimental; urgency=medium

  * Initial release (Closes: #794085)

 -- Pirate Praveen <praveen@debian.org>  Thu, 30 Jul 2015 17:16:58 +0530
